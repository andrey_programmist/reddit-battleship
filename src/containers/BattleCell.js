import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Fire } from '../redux/actions';
import { PositionToString } from '../utils/mappers';
import { SHOTRESULT_HIT, SHOTRESULT_MISS, SHOTRESULT_WAITING } from '../utils/shotResults';
import './BattleCell.css';

export const BattleCellPure = ({ shotResult, actions, position }) => {
  const cellClasses = classnames({
    BattleCell: true,
    [shotResult]: true,
  });
  const cell = shotResult === SHOTRESULT_WAITING ? (
    <div className={cellClasses} onClick={() => actions.Fire(position)} role="none" />
  ) :
    (
      <div className={cellClasses} role="none" />
    );
  return cell;
};

BattleCellPure.propTypes = {
  shotResult: PropTypes.string.isRequired,
  actions: PropTypes.objectOf(PropTypes.func).isRequired,
  position: PropTypes.arrayOf(PropTypes.number).isRequired,
};

const mapStateToProps = (state, { position }) => {
  const bc = state.battlefield[PositionToString(position)]; // battle cell
  return {
    shotResult: (bc === undefined && SHOTRESULT_WAITING) ||
      (!bc && SHOTRESULT_MISS) ||
      SHOTRESULT_HIT,
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ Fire }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(BattleCellPure);
