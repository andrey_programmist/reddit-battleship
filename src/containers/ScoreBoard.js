import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { PlayerToString } from '../utils/mappers';
import './ScoreBoard.css';

const ScoreBoard = ({ left, right }) => {
  const players = [left, right].map(({ side, scores, title }) => (
    <div className={classnames([side, 'ScoreItem'])} key={PlayerToString(side, title)}>
      <div className="ScoreValue">{`${scores}`.padStart(2, '0')}</div>
      <div className="ScorePlayer">{title}</div>
    </div>
  ));
  return (
    <div className="ScoreBoard">{players}</div>
  );
};
const playerShape = {
  side: PropTypes.string,
  scores: PropTypes.number,
  title: PropTypes.string,
};
ScoreBoard.propTypes = {
  left: PropTypes.shape(playerShape).isRequired,
  right: PropTypes.shape(playerShape).isRequired,
};

const mapStateToProps = state => ({ ...state.scoreboard });

export default connect(mapStateToProps)(ScoreBoard);
