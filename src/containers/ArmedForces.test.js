import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import ArmedForces from './ArmedForces';
import Unit from './Unit';
import * as initialState from '../redux/initialState.json';
import * as extendedState from '../redux/extendedState.json';

describe('ArmedForces', () => {
  let wrapper;

  beforeEach(() => {
    const store = configureStore()({ ...initialState, ...extendedState });
    wrapper = mount(<Provider store={store}><ArmedForces /></Provider>);
  });
  it('should be rendered', () => {
    expect(wrapper.length).toEqual(1);
  });
  it('should contains valid units count', () => {
    expect(wrapper.find(Unit).length).toEqual(initialState.layout.length);
  });
});
