import React from 'react';
import { shallow } from 'enzyme';
import { BattleCellPure as BattleCell } from './BattleCell';
import { SHOTRESULT_HIT, SHOTRESULT_MISS, SHOTRESULT_WAITING } from '../utils/shotResults';

describe('BattleCell', () => {
  it('should be rendered', () => {
    const bc = (
      <BattleCell
        position={[0, 0]}
        actions={{ Fire: e => e }}
        shotResult={SHOTRESULT_WAITING}
      />
    );
    const wrapper = shallow(bc);
    expect(wrapper.length).toEqual(1);
    expect(wrapper.find('.BattleCell').length).toEqual(1);
  });

  it(`should handle click action while ${SHOTRESULT_WAITING} state`, () => {
    const mockFn = jest.fn();
    const bc = (
      <BattleCell
        position={[0, 0]}
        actions={{ Fire: mockFn }}
        shotResult={SHOTRESULT_WAITING}
      />
    );
    const wrapper = shallow(bc);
    expect(mockFn.mock.calls.length).toEqual(0);
    wrapper.find('.BattleCell').simulate('click');
    expect(mockFn.mock.calls.length).toEqual(1);
  });

  it(`should handle click action while ${SHOTRESULT_MISS} state`, () => {
    const mockFn = jest.fn();
    const bc = (
      <BattleCell
        position={[0, 0]}
        actions={{ Fire: mockFn }}
        shotResult={SHOTRESULT_MISS}
      />
    );
    const wrapper = shallow(bc);
    expect(mockFn.mock.calls.length).toEqual(0);
    wrapper.find('.BattleCell').simulate('click');
    expect(mockFn.mock.calls.length).toEqual(0);
  });

  it(`should handle click action while ${SHOTRESULT_HIT} state`, () => {
    const mockFn = jest.fn();
    const bc = (
      <BattleCell
        position={[0, 0]}
        actions={{ Fire: mockFn }}
        shotResult={SHOTRESULT_HIT}
      />
    );
    const wrapper = shallow(bc);
    expect(mockFn.mock.calls.length).toEqual(0);
    wrapper.find('.BattleCell').simulate('click');
    expect(mockFn.mock.calls.length).toEqual(0);
  });
});
