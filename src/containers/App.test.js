import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import App from './App';
import * as initialState from '../redux/initialState.json';
import * as extendedState from '../redux/extendedState.json';

it('should be rendered', () => {
  const store = configureStore()({ ...initialState, ...extendedState });
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={store}><App /></Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
