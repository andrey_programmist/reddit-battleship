import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Unit, { GetStatusBlockFromSizes } from './Unit';
import { UnitToString } from '../utils/mappers';
import './ArmedForces.css';

const ArmedForces = ({ shipTypes, shipUnits }) => {
  const sizedStatusBlocks = GetStatusBlockFromSizes(Object.values(shipTypes).map(i => i.size));
  const units = shipUnits.filter(unit => shipTypes[unit.ship]).map(unit => (
    <Unit
      type={unit.ship}
      key={UnitToString(unit.ship, unit.positions)}
      positions={unit.positions}
      statusBlock={sizedStatusBlocks[shipTypes[unit.ship].size]}
    />
  ));
  return (
    <div className="ArmedForces">{units}</div>
  );
};
const shipShape = {
  size: PropTypes.number.isRequired,
  count: PropTypes.number.isRequired,
};
const unitShape = {
  ship: PropTypes.string.isRequired,
  positions: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)),
};
ArmedForces.propTypes = {
  shipTypes: PropTypes.objectOf(PropTypes.shape(shipShape)).isRequired,
  shipUnits: PropTypes.arrayOf(PropTypes.shape(unitShape)).isRequired,
};
const mapStateToProps = state => ({
  shipTypes: state.shipTypes,
  shipUnits: state.layout,
});

export default connect(mapStateToProps)(ArmedForces);
