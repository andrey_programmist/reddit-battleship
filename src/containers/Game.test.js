import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import Game from './Game';
import InfoBoard from '../components/InfoBoard';
import BattleField from '../components/BattleField';
import * as initialState from '../redux/initialState.json';
import * as extendedState from '../redux/extendedState.json';

describe('Game', () => {
  let wrapper;

  beforeEach(() => {
    const store = configureStore()({ ...initialState, ...extendedState });
    wrapper = mount(<Provider store={store}><Game /></Provider>);
  });
  it('should be rendered', () => {
    expect(wrapper.length).toEqual(1);
  });
  it('should contains valid elements', () => {
    expect(wrapper.find(InfoBoard).length).toEqual(1);
    expect(wrapper.find(BattleField).length).toEqual(1);
  });
});
