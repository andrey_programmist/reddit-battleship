import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import BattleField from '../components/BattleField';
import InfoBoard from '../components/InfoBoard';
import './Game.css';

const Game = ({ settings }) => (
  <div className="Game">
    <InfoBoard />
    <BattleField dimensions={settings.dimensions} />
  </div>
);

const settingsShape = {
  dimensions: PropTypes.arrayOf(PropTypes.number).isRequired,
};
Game.propTypes = {
  settings: PropTypes.shape(settingsShape).isRequired,
};

const mapStateToProps = state => ({ settings: state.settings });

export default connect(mapStateToProps)(Game);
