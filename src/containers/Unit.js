import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { PositionToString, SizedStatusBlockToString } from '../utils/mappers';
import './Unit.css';

export const GetStatusBlockFromSizes = sizes => sizes.reduce((acc, size) => {
  if (!acc[size]) {
    acc[size] = [...Array(size).keys()].map(i => (
      <img key={SizedStatusBlockToString(size, i)} alt="" />
    ));
  }
  return acc;
}, {});

const Unit = ({
  type, isWasted, statusBlock,
}) =>
  (
    <div className={classnames({ Unit: true, [type]: true, wasted: isWasted })}>
      <div className="UnitType">
        <img alt="" />
      </div>
      <div className="UnitStatus">
        {statusBlock}
      </div>
    </div>
  );
Unit.propTypes = {
  type: PropTypes.string.isRequired,
  isWasted: PropTypes.bool,
  statusBlock: PropTypes.arrayOf(PropTypes.element).isRequired,
};
Unit.defaultProps = {
  isWasted: false,
};

const mapStateToProps = (state, { positions }) => ({
  isWasted: positions.every(p => state.battlefield[PositionToString(p)]),
});
export default connect(mapStateToProps)(Unit);
