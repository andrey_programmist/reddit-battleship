import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import Unit, { GetStatusBlockFromSizes } from '../containers/Unit';
import * as initialState from '../redux/initialState.json';
import * as extendedState from '../redux/extendedState.json';
import { PositionToString } from '../utils/mappers';

describe('Unit', () => {
  const type = 'destroyer';
  const positions = [[0, 1], [0, 2]];
  const sizedBlocks = GetStatusBlockFromSizes([2, 3, 4, 5]);
  const props = {
    type,
    statusBlock: sizedBlocks[positions.length],
    positions,
  };

  let store;

  beforeEach(() => {
    store = configureStore()({
      ...initialState,
      ...extendedState,
      battlefield: {
        [PositionToString(positions[0])]: true,
        [PositionToString(positions[1])]: true,
      },
    });
  });
  it('should be rendered', () => {
    const wrapper = mount(<Provider store={store}><Unit {...props} /></Provider>);
    expect(wrapper.find('.Unit').length).toEqual(1);
  });
  it('should be wasted', () => {
    const tProps = { ...props };
    const wrapper = mount(<Provider store={store}><Unit {...tProps} /></Provider>);
    expect(wrapper.find('.Unit').hasClass('wasted')).toEqual(true);
  });
  it('should has type css-class', () => {
    const wrapper = mount(<Provider store={store}><Unit {...props} /></Provider>);
    expect(wrapper.find('.Unit').hasClass(props.type)).toEqual(true);
  });
  it('should has multiple status images', () => {
    const wrapper = mount(<Provider store={store}><Unit {...props} /></Provider>);
    expect(wrapper.find('.Unit .UnitStatus img').length).toEqual(props.positions.length);
  });
});
