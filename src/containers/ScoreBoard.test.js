import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import ScoreBoard from './ScoreBoard';
import * as initialState from '../redux/initialState.json';
import * as extendedState from '../redux/extendedState.json';

describe('ScoreBoard', () => {
  let wrapper;

  beforeEach(() => {
    const store = configureStore()({ ...initialState, ...extendedState });
    wrapper = mount(<Provider store={store}><ScoreBoard /></Provider>).find(ScoreBoard);
  });

  it('should be rendered', () => {
    expect(wrapper.length).toEqual(1);
  });
  it('should contains two players', () => {
    expect(wrapper.find('.ScoreItem').length).toEqual(2);
  });
  it('should be show padded (2) score value', () => {
    expect(wrapper.find('.ScoreValue').first().text().length).toEqual(2);
  });
});
