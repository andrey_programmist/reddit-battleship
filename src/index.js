import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { install } from 'react-loop';
import App from './containers/App';
import reducer from './redux/reducers';
import './index.css';

import * as initialState from './redux/initialState.json';
import * as extendedState from './redux/extendedState.json';

const store = createStore(reducer, { ...initialState, ...extendedState }, install());
const app = (
  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
