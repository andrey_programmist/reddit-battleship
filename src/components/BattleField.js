import React from 'react';
import PropTypes from 'prop-types';
import BattleCell from '../containers/BattleCell';
import { PositionToString } from '../utils/mappers';
import './BattleField.css';

const BattleField = ({ dimensions }) => {
  const cells = [];
  for (let y = 0; y < dimensions[1]; y += 1) {
    for (let x = 0; x < dimensions[0]; x += 1) {
      const pos = [x, y];
      cells.push(<BattleCell key={PositionToString(pos)} position={pos} />);
    }
  }
  const gridStyle = {
    gridTemplateColumns: `repeat(${dimensions[0]},1fr)`,
    gridTemplateRows: `repeat(${dimensions[1]},1fr)`,
  };
  return (
    <div className="BattleField" style={gridStyle}>{cells}</div>
  );
};
BattleField.propTypes = {
  dimensions: PropTypes.arrayOf(PropTypes.number).isRequired,
};

export default BattleField;
