import React from 'react';
import ScoreBoard from '../containers/ScoreBoard';
import ArmedForces from '../containers/ArmedForces';
import './InfoBoard.css';

const InfoBoard = () => (
  <div className="InfoBoard">
    <ScoreBoard />
    <ArmedForces />
  </div>
);

export default InfoBoard;
