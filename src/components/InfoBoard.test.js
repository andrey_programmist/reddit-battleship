import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import InfoBoard from './InfoBoard';
import ScoreBoard from '../containers/ScoreBoard';
import ArmedForces from '../containers/ArmedForces';
import * as initialState from '../redux/initialState.json';
import * as extendedState from '../redux/extendedState.json';

describe('InfoBoard', () => {
  let wrapper;

  beforeEach(() => {
    const store = configureStore()({ ...initialState, ...extendedState });
    wrapper = mount(<Provider store={store}><InfoBoard /></Provider>);
  });
  it('should be rendered', () => {
    expect(wrapper.length).toEqual(1);
  });
  it('should contains valid elements', () => {
    expect(wrapper.find(ScoreBoard).length).toEqual(1);
    expect(wrapper.find(ArmedForces).length).toEqual(1);
  });
});
