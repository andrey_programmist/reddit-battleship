import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import BattleField from './BattleField';
import BattleCell from '../containers/BattleCell';
import * as initialState from '../redux/initialState.json';
import * as extendedState from '../redux/extendedState.json';

describe('BattleField', () => {
  let wrapper;

  beforeEach(() => {
    const store = configureStore()({ ...initialState, ...extendedState });
    wrapper = mount(<Provider store={store}><BattleField dimensions={[10, 10]} /></Provider>);
  });
  it('should be rendered', () => {
    expect(wrapper.length).toEqual(1);
  });
  it('should contains valid cells count', () => {
    const dims = wrapper.find(BattleField).prop('dimensions');
    expect(wrapper.find(BattleCell).length).toEqual(dims[0] * dims[1]);
  });
});
