import { createStore } from 'redux';
import { install } from 'redux-loop';
import { Fire } from './actions';
import reducer from './reducers';
import { PositionToString } from '../utils/mappers';
import * as primaryState from './initialState.json';
import * as extendedState from './extendedState.json';

const initialState = Object.assign({}, primaryState, extendedState);
it('should test rootReducer for valid state structure but not types', () => {
  const state = Object.assign({}, initialState);

  const properties = [
    'layout',
    'shipTypes',
    'scoreboard',
    'scoreboard.left',
    'scoreboard.right',
    'scoreboard.left.side',
    'scoreboard.left.scores',
    'scoreboard.left.title',
    'scoreboard.right.side',
    'scoreboard.right.scores',
    'scoreboard.right.title',
    'settings',
    'settings.dimensions',
    'battlefield',
  ];
  properties.forEach(prop => expect(state).toHaveProperty(prop, expect.anything()));
});
it('should test rootReducer Fire action - cell used', async () => {
  const position = [0, 0];
  const prevState = Object.assign({}, initialState, {
    battlefield: { [PositionToString(position)]: true },
  });

  const store = createStore(reducer, prevState, install())

  await store.dispatch(Fire(position))

  const nextState = store.getState();

  expect(nextState).toEqual(prevState);
});

it('should test rootReducer Fire action - cell hited', async () => {
  const position = [0, 1];
  const prevState = Object.assign({}, initialState, {
    layout: [{ positions: [[0, 1]] }],
  });

  const store = createStore(reducer, prevState, install())

  await store.dispatch(Fire(position))

  const nextState = store.getState();

  expect(nextState).toHaveProperty(`battlefield.${PositionToString(position)}`, true);
});

it('should test rootReducer Fire action - cell hited', async () => {
  const position = [0, 1];
  const prevState = Object.assign({}, initialState, {
    layout: [{ positions: [[0, 0]] }],
  });

  const store = createStore(reducer, prevState, install())

  await store.dispatch(Fire(position))

  const nextState = store.getState();

  expect(nextState).toHaveProperty(`battlefield.${PositionToString(position)}`, false);
});
