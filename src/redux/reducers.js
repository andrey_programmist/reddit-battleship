import combiner from 'redux-combiner';
import { combineReducers, loop, Cmd } from 'redux-loop';
import { UPDATE_LAYOUT, UPDATE_SHIPTYPES, BATTLE_FIRE, HIT_CELL, SCORE_LEFT } from './actions';
import { PositionToString } from '../utils/mappers';

const { node } = combiner(combineReducers);

const checkBattleCell = (ships, position) =>
  ships.some(({ positions }) => positions
    .some(p => p[0] === position[0] && p[1] === position[1]));

const getPayload = (state, { payload }) => payload

const rootReducer = node({

  shipTypes: node({}).on(UPDATE_SHIPTYPES, getPayload),

  layout: node({})
    .on(UPDATE_LAYOUT, getPayload)
    .on(BATTLE_FIRE, (layout, { position }) =>
      loop(layout, Cmd.action({
        type: HIT_CELL,
        pos: PositionToString(position),
        isCellHited: checkBattleCell(layout, position)
      }))),

  battlefield: node({})
    .on(HIT_CELL, (battlefield, { pos, isCellHited }) => {
      if (battlefield[pos] !== undefined) { return battlefield }
      const nextBattlefield = { ...battlefield, [pos]: isCellHited }
      return isCellHited
        ? loop(nextBattlefield, Cmd.action({ type: SCORE_LEFT }))
        : nextBattlefield
    }),

  scoreboard: node({
    left: {
      scores: node(0).on(SCORE_LEFT,scores => scores + 1),
    }
  })
})

export default rootReducer;
