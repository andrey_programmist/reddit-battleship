import { Fire, BATTLE_FIRE } from './actions';

it(`should test the ${BATTLE_FIRE}`, () => {
  const position = [0, 0];
  expect(Fire(position)).toMatchObject({ type: BATTLE_FIRE, position });
});
